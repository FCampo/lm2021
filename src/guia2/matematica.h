#ifndef G2_MATEMATICA_H
#define G2_MATEMATICA_H

#include <stdint.h>
#include <stdio.h>

#define SAT16_MAX 100
#define SAT16_MIN -100

int32_t Sumar_Array_Guia2(int16_t* x, int16_t xn);

int16_t Multiplicar_Sat(int16_t n1, int16_t n2);

static inline int16_t Multiplicar_Sat_Inline(int16_t n1, int16_t n2){
	int16_t res=n1*n2;
	if(res>SAT16_MAX)
		res=SAT16_MAX;
	if(res<SAT16_MIN)
		res=SAT16_MIN;
	return res;
}

//* Ejercicio 5

uint16_t Cuenta_Accesos_G2(void);

#endif