#include "matematica.h"

int32_t Sumar_Array_Guia2(int16_t* x, int16_t xn){
	int32_t aux=0;
	for(int16_t i=0; i<xn; i++){
		aux+=x[i];
	}
	return aux;
}

int16_t Multiplicar_Sat(int16_t n1, int16_t n2){
	int16_t res=n1*n2;
	if(res>SAT16_MAX)
		res=SAT16_MAX;
	if(res<SAT16_MIN)
		res=SAT16_MIN;
	return res;
}

//* Ejercicio 5

extern char end;

uint16_t Cuenta_Accesos_G2(void){

	#ifndef CANT_DEF //! Si cant no esta definida, la definimos con un valor igual a 0
	#define CANT_DEF
		static uint16_t cant=0;
	#endif

	printf("Direccion de memoria de bss: 0x%X\n", &end);
	printf("Direccion de memoria de la varaible estatica cant: 0x%X\n", &cant);
	return ++cant;
}