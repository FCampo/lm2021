#ifndef DRIVERS_H
#define DRIVERS_H

#include <stdint.h>

#define OK 1
#define ERROR 0

//? Supongo que todo esto viene implementado por otros archivos.

extern void prender_led(uint8_t led);
extern void apagar_led(uint8_t led);

extern char* recibir_usuario(); // Supongo que la funcion devuelve esto... (se recibe usuario) ? reutrn nombre_usuario : return NULL;
extern uint8_t recibir_clave(); // Supongo que la funcion devuelve esto... (la pass coincide con la del usuario) ? return OK : return ERROR;

extern void init_sistema_iniciado_mde(); // Inicializacion de la maquina de estado con el sistema iniciado
extern void sistema_iniciado_mde(); // Maquina de estado del sistema iniciado

extern uint16_t contador_ms; // Contador incrementado por una interrupcion externa

void delay_ms(uint16_t delay){ // Función de Delay no estoy 100% seguro de que este bien implementada
	contador_ms=0;
	while(contador_ms<=delay) asm("nop");
}

void parpadeo_led(uint8_t led){
	prender_led(led);
	delay_ms(100);
	apagar_led(led);
}

#endif //DRIVERS_H