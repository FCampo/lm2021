#include "g3e4.h"

//? Muchas de las funciones utilizadas estan declaradas como extern en drivers.h y se encuentra explicado su supuesto funcionamiento

void init_mde(void){
	strcpy(usuario, "\0");
	strcpy(usuarioAnterior, "\0");
	estado=APAGADO;
	nClaveError=0;
	apagar_led(LED1);
	apagar_led(LED2);
	init_sistema_iniciado_mde();
	pMde=&mde;
}

void mde(void){
	switch (estado){
		case APAGADO:
			parpadeo_led(LED2);
			delay_ms(500);
			parpadeo_led(LED2);
			estado=ESPERO_USUARIO;
		break;
		
		case ESPERO_USUARIO:
			strcpy(usuario, recibir_usuario());
			if(usuarioAnterior!=usuario){
				strcpy(usuarioAnterior, usuario);
				nClaveError=0;
			}
			if(usuario!=NULL)
				estado=ESPERO_CLAVE;
		break;
		
		case ESPERO_CLAVE:
			uint8_t retClave = recibir_clave();
			if(retClave==ERROR){
				nClaveError++;
				if(nClaveError>=NRO_CLAVE_ERROR)
					estado=SISTEMA_BLOQUEADO;
				else estado=APAGADO;
				break;
			}
			if(retClave==OK){
				parpadeo_led(LED1);
				nClaveError=0;
				estado=SISTEMA_INICIADO;
			}
		break;
		
		case SISTEMA_BLOQUEADO:
			prender_led(LED2);
		break;
		
		case SISTEMA_INICIADO:
			init_sistema_iniciado_mde();
			pMde=&sistema_iniciado_mde;
		break;

		default:
			init_mde();
		break;
	}
}