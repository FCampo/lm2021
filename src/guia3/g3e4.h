#ifndef G3E4_H
#define G3E4_h

#include <stdio.h>
#include "drivers.h"
#include "string.h"

#define LED1 1
#define LED2 2
#define NRO_CLAVE_ERROR 3
#define OK 1
#define ERROR 0

typedef enum Estado_typedef {APAGADO, ESPERO_USUARIO, ESPERO_CLAVE, SISTEMA_INICIADO, SISTEMA_BLOQUEADO} Estado;

uint8_t nClaveError=0;
Estado estado;

char usuario[24]="\0";
char usuarioAnterior[24]="\0";

void init_mde(void);

void mde(void);

void (*pMde)(void)=&mde; // Puntero que controla la maquina de estado funcionando actualmente

#endif //G3E4_H